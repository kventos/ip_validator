from prettytable import PrettyTable
from cmd import Cmd


class ValidateIPAddress:
    def __init__(self):
       self.hex = "1234567890abcdefABCDEF"
       self.table = PrettyTable(["IP", "Verdict"])
       self.db = []

    def validateIPAddress(self, ips):
        for ip in ips:
            if ip.count(".") == 3:
                verdict = self.validateIPv4(ip)
            elif ip.count(":") == 7:
                verdict = self.validateIPv6(ip)
            else:
                verdict = "WRONG"
            self.db.append([ip, verdict])
            self.table.add_row([ip, verdict])
        print(self.table)
        self.table.clear_rows()
        return self.db
        
    def validateIPv4(self, ip):
        for word in ip.split("."):
            if not word.isdigit() or str(int(word)) != word or int(word) > 255 or int(word) < 0:
                return "Wrong IPv4"
        return "Valid IPv4"
        
    def validateIPv6(self, ip):
        for word in ip.split(":"):
            if not word or not word.isalnum() or len(word) > 4 \
                or any(char not in self.hex for char in word):
                return "Wrong IPv6"
        return "Valid IPv6"
    
        

class ValidateCmd(Cmd):
    validator = ValidateIPAddress()

    def do_validate(self, args):
        args = args.split(' ')
        if len(args) == 0:
            print("Provide your IPs")
        else:
            self.validator.validateIPAddress(args)
    
def main():
    validator = ValidateCmd()
    validator.prompt = '> '
    validator.cmdloop('Start IP validator...')

if __name__ == '__main__':
    main()
