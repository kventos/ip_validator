from setuptools import setup

__author__ = 'Daniil Smirnov'
setup(
    name='ip_validator',
    version='1.0.0',
    description='Ipv4 ipv6 validator',
    url='https://gitlab.com/kventos/ip_validator',
    author='Daniil Smirnov',
    author_email='sdan1995@mail.ru',
    packages=['validator'],
    install_requires=[
       'prettytable', 'pytest'
    ],
    entry_points={
        'console_scripts' : ['ip_validator = validator.ip_validator:main']
    }
)

