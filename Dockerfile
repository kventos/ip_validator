FROM ubuntu:16.04 as ip_validator_for_debian
WORKDIR /Users/dr.smirnov/Documents/ip_validator
COPY . /app
RUN apt-get update \
&& apt-get install -y software-properties-common \
&& add-apt-repository ppa:jonathonf/python-3.6 \
&& apt-get update \
&& apt-get install -y python3 python3-setuptools python3-pip fakeroot python-stdeb python3-all \
&& pip3 install --trusted-host pypi.python.org -r /app/requirements.txt


FROM centos/python-36-centos7 as ip_validator_for_centos
WORKDIR /Users/dr.smirnov/Documents/ip_validator
COPY . /app
USER root
RUN yum install -y rpm-build rpm-devel rpmlint make python bash coreutils diffutils rpmdevtools
RUN pip3 install --trusted-host pypi.python.org -r /app/requirements.txt
