import unittest
from validator import ip_validator


class TestValidateIPAddress(unittest.TestCase):
    def setUp(self):
        self.validator = ip_validator.ValidateIPAddress()
        
    def test_ipv6_right(self):
        result = self.validator.validateIPAddress(["2001:0db8:85a3:0:0:8A2E:0370:7334"])
        self.assertEqual(result[0][1], "Valid IPv6")

    def test_ipv4_0(self):
        result = self.validator.validateIPAddress(["0.0.0.0"])
        self.assertEqual(result[0][1], "Valid IPv4")

    def test_ipv4_256(self):
        result = self.validator.validateIPAddress(["256.256.256.256"])
        self.assertEqual(result[0][1], "Wrong IPv4")

    def test_ipv4_letter(self):
        result = self.validator.validateIPAddress(["56.abc.78.11"])
        self.assertEqual(result[0][1], "Wrong IPv4")

    def test_batch_right(self):
        result = self.validator.validateIPAddress(["2001:0db8:85a3:0:0:8A2E:0370:7334", "10.70.84.123", "0.0.0.0", "2001:0d54b8:85a3:0:0:8vgA2E:0370:7334"])
        self.assertEqual(result[0][1], "Valid IPv6")
        self.assertEqual(result[1][1], "Valid IPv4")
        self.assertEqual(result[2][1], "Valid IPv4")
        self.assertEqual(result[3][1], "Wrong IPv6")

    def test_ipv6_invalid_longer(self):
        result = self.validator.validateIPAddress(["2001:0db8:85a3:0:0:8A2E:0370:7334sffrg"])
        self.assertEqual(result[0][1], "Wrong IPv6")

    def test_ipv6_invalid_special_syml(self):
        result = self.validator.validateIPAddress(["2001:0db8:85a3:0:0:8!2E:0370:7334"])
        self.assertEqual(result[0][1], "Wrong IPv6")

    def test_fully_wrong(self):
        result = self.validator.validateIPAddress(["2001:0db8:85a3:0:0:8!2E"])
        self.assertEqual(result[0][1], "WRONG")

    def test_ipv6_not_full(self):
        result = self.validator.validateIPAddress(["2001:0db8:85a3:0:0:8!2E:85a3:"])
        self.assertEqual(result[0][1], "Wrong IPv6")

